const loaded = [];

function fakeAMDloader() {
  const receivers = [];

  function define(id, dependencies, factory) {
    const module = factory();
    loaded.push({ id, module });
    receivers.forEach((receiver) => receiver({ id, module }));
  }
  define.amd = true;
  window.define = define;

  return {
    subscribe: (receiver) => {
      receivers.push(receiver);
    },
  }
}

const amd = fakeAMDloader();

function getModuleById(id) {
  for (let i = 0; i < loaded.length; i += 1) {
    if (loaded[i].id === id) {
      return loaded[i].module;
    }
  }
  return null;
}

function waitFor(ids, callback) {
  const neededIds = Array.isArray(ids) ? ids : [ids,];
  const loadedIds = [];

  function moduleArrived({ id, module }) {
    const index = neededIds.indexOf(id);
    if (index !== -1) {
      loadedIds.push(id);
      if (loadedIds.length === neededIds.length) {
        const modules = neededIds.map(getModuleById);
        callback(modules.length === 1 ? modules[0] : modules)
      }
    }
  }

  loaded.forEach(moduleArrived);
  amd.subscribe(moduleArrived);
}


waitFor('jquery', ($) => {
    $('body').html('hello');
});

waitFor(['jquery', 'underscore'], ([$, _]) => {
  _.delay(() => {
    console.log($('body').html())
  }, 1000);
});
